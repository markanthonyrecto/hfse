'use strict';
var hfse = {
	width: 0,
	height: 0,
	isMobile: false,
	init: function() {
		hfse.mobileDetect();
		// for mobile
		if (hfse.isMobile) {
			document.querySelector('html').classList.add('bp-touch');
		}

		hfse.resizeFn(function() {
			hfse.resize();
		});

		hfse.inview();

		hfse.loadFn(function() {
			hfse.ready();
		});

		hfse.header();

		$('.list-hover').each(function() {
			hfse.listHover({target: this});
		});

		$('.com_masthead').each(function() {
			var _this = this;

			var _mobileHeight = 0;
			hfse.resizeFn(function() {
				// if regular size screen, occupy the full height
				if(hfse.width < 1920 || hfse.width == 1920) {
					if(hfse.width > 640) {
						var _height = window.innerHeight - hfse.getHeight(document.querySelector('.header-content'));
						if(_height < 500) {
							_height = 500;
						}

						$(_this).css({minHeight: _height});
					} else {
						// safari, has diff height upon scroll, must use only 1 height
						if(window.innerHeight > _mobileHeight) {
							_mobileHeight = window.innerHeight;
						}
						
						if(_mobileHeight > 500) {
							_mobileHeight = 500;
						}

						$(_this).css({minHeight: _mobileHeight - hfse.getHeight(document.querySelector('.header-content'))});
					}
				} else {
					// if on bigger screen, use the css minHeight
					$(_this).css({minHeight: ''});
				}
			});
		});

		$('.com_tabs').each(function(index) {
			$(this).tabs();
		});

		$('.com_mobile-dropdown').each(function() {
			var _this = this;

			hfse.enquire({
				between: 768,
				desktop: function() {
					hfse.mobileDropdown({target: _this, destroy: true});
				},
				mobile: function() {
					hfse.mobileDropdown({target: _this});
				}
			})
		});

		// for accordion
		$('.com_accordion').each(function() {
			var _closeOthers = this.getAttribute('close-others'),
			_openFirst = this.getAttribute('open-first');

			hfse.accordion({
				target: this,
				closeOthers: _closeOthers,
				openFirst: _openFirst
			});
		});

		$('.ab-co-accordion').each(function() {
			var _sets = this.querySelectorAll('.com_accordion-set');

			for (var i = 0; i < _sets.length; i++) {
				var _thisSet = _sets[i],
				_number = _thisSet.querySelectorAll('.ac-acc-num');

				if(_number.length != 0) {
					for (var j = 0; j < _number.length; j++) {
						if(j == 0) {
							$(_number[j]).html(i + 1);
						} else {
							$(_number[j]).html(parseInt(i + 1) + '.' + j);
						}
					}
				}
			}
		});

		$('.com_mobile-scollable').each(function() {
			var _this = this,
			_column = parseInt(_this.getAttribute('data-columns'));

			hfse.enquire({
				between: 768,
				desktop: function() {
					hfse.mobileScrollable({target: _this, destroy: true});
				},
				mobile: function() {
					hfse.mobileScrollable({target: _this, columns: _column});
				}
			});
		});
	},
	ready: function() {
		// call the resize function upen page ready
		setTimeout(function() {
			hfse.dispatchEvent(window, 'resize');
		}, 200);

		$('.com_video-player').each(function() {
			var _this = this,
			_video = _this.querySelector('video');

			if(_video) {
				_video.autoplay = true;
				_video.loop = true;
				_video.muted = true;
			}
		});

		$('.slider-animate').each(function() {
			var _index = 0.3;
			$('> *', this).each(function() {
				_index = _index + 0.3;
				$(this).css({animationDelay: _index + 's'});
			});
		});

		$('.com_ho-slider .flexslider').flexslider({
			animation: 'slide',
			directionNav: false,
			controlNav: true
		});

		// for mobile carousel
		$('.com_mobile-carousel').each(function() {
			var _this = this,
			_type = this.getAttribute('carousel-type'),
			_items = this.querySelectorAll('.item');

			for (var i = 0; i < _items.length; i++) {
				_items[i].setAttribute('data-number', i);
			}

			hfse.enquire({
				between: 810,
				desktop: function() {
					$(_this).closest('.section').css({position: '', overflow: ''});

					$(_this).removeClass('owl-carousel');

					$(_this).owlCarousel('destroy');
				},
				mobile: function() {
					$(_this).closest('.section').css({position: 'relative', overflow: 'hidden'});

					$(_this).addClass('owl-carousel');

					if(_type == '2') {
						$(_this).owlCarousel({
							items: 2,
							margin: 15,
							nav: false,
							dots: false,
							smartSpeed: 700,
							responsive: {
								0: {
									items: 2
								},
								480: {
									items: 2
								}
							}
						});
					} else {
						$(_this).owlCarousel({
							items: 1,
							margin: 15,
							nav: false,
							dots: false,
							smartSpeed: 700,
							responsive: {
								0: {
									items: 1
								},
								550: {
									items: 2
								}
							}
						});
					}

					// move to active slide
					var _active = _this.querySelector('.item.active');
					if(_active) {
						var _number = parseInt(_active.getAttribute('data-number'));

						if(_type == '2') {
							_number--;
						}

						$(_this).trigger('to.owl.carousel', _number);
					}
				}
			});

			hfse.resizeFn(function() {
				if(hfse.width < 810 || hfse.width == 810) {
					var _width = $(_this).closest('.container').outerWidth() + ((window.innerWidth - $(_this).closest('.container').outerWidth()) / 2) + 15;
					$(_this).css({width: _width});
				} else {
					$(_this).css({width: ''});
				}
			});
		});

		$('.com_middle-content').each(function() {
			var _this = this;

			hfse.resizeFn(function() {
				var _height = window.innerHeight - (hfse.getHeight(document.querySelector('.header-content')));

				$('> div', _this.parentNode).each(function() {
					var _sibling = this;

					_height = _height - _sibling.offsetHeight;
					
				});

				_height = _height - ( parseInt($(_this).closest('.section').css('padding-top')) );
				if(_height < 0) {
					_height = 0;
				}

				$(_this).css({marginTop: _height / 2, marginBottom: _height / 2});
			});
		});
	},
	loadFn: function(fnctn) {
		if(document.readyState === 'interactive' || document.readyState === 'complete') {
        	return fnctn();
        } else if(document.addEventListener) {
        	document.addEventListener('DOMContentLoaded', function() {
              return fnctn();
          	});
        } else if (document.attachEvent) {
        	document.attachEvent("onreadystatechange", function() {
            	if(document.readyState != 'loading') {
                	return fnctn();
                }
            });
        }
	},
	resizeFn: function(fnctn) {
		var _timer;

		fnctn();

		window.addEventListener('resize', function() {
			fnctn();
			clearTimeout(_timer);
			_timer = setTimeout(function() {
				fnctn();
			}, 200);
		});
	},
	scrollFn: function(fnctn) {
		fnctn();
		window.addEventListener('scroll', function() {
			fnctn();
		});
	},
	dispatchEvent: function(elem, eventName) {
		var event;
		if (typeof(Event) === 'function') {
			event = new Event(eventName);
		}
		else {
			event = document.createEvent('Event');
			event.initEvent(eventName, true, true);
		}
		elem.dispatchEvent(event);
	},
	getHeight: function(target) {
		if(target) {
			return target.offsetHeight;
		} else {
			return 0;
		}
	},
	resize: function() {
		var _resize = {
			init: function() {
				hfse.width = window.innerWidth;
				hfse.height = window.innerHeight;

				// STICKY FOOTER
				var header = document.querySelector('.header-content'),
				footer = document.querySelector('footer');
				
				$(footer).css({marginTop: -(hfse.getHeight(footer))});
				$('#main-wrapper').css({paddingBottom: hfse.getHeight(footer)});
				$('.com_header-height').css({paddingTop: hfse.getHeight(header)});

				// for equal height
				$('.group-height').each(function() {
					hfse.equalize(this.querySelectorAll('.gh1'));
					hfse.equalize(this.querySelectorAll('.gh2'));
					hfse.equalize(this.querySelectorAll('.gh3'));
					hfse.equalize(this.querySelectorAll('.gh4'));
					hfse.equalize(this.querySelectorAll('.gh5'));
					hfse.equalize(this.querySelectorAll('.gh6'));
				});
			}
		}
		_resize.init();
	},
	equalize: function(target) {
		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = 0;
		}

		var _biggest = 0;
		for (var i = 0; i < target.length; i++ ){
			var element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	mobileDetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			hfse.isMobile = isMobile.any();
		}
	},
	inview: function(state) {
		var _animate = {
			init: function() {
				$('.animate').each(function() {
					var _state = true,
					_this = $(this),
					_animControl = _this.attr('anim-control'),
					_animName = _this.attr('anim-name'),
					_animDelay = Math.floor(_this.attr('anim-delay')),
					_delayIncrease = 0.2,
					_delayNum = 0;

					// set the css animation name
					if(!(_animName)) {
						_animName = 'anim-content';
					} else {
						// if there is a special animation name
						_animName = _animName + ' animated';
					}

					// set the animation delay
					if(!(_animDelay)) {
						// if there is no attr for delay
						_animDelay = 0.5;
					}
					_delayNum = _animDelay + 's';

					if(_animControl == 'parent') {
						// for parent container
						var _childen = $('> *', _this);

						_childen.each(function(i) {
							var _child = $(this);

							// multiply the delay depending on child count
							_delayNum = (_animDelay + (_delayIncrease * i)) + 's';

							_child.css('animation-delay', _delayNum);

							// once the target is on display
							$(_this).on('inview', function(event, visible) {
								if(_state) {
									_state = false;

									if(visible) {
										$(_this).removeClass('animate');
										_childen.addClass('animate');
										_childen.addClass(_animName + ' visible');

										// once done, remove everything related to animation
										setTimeout(function() {
											_childen.removeClass('animate animated anim-content visible');
											_childen.removeClass(_animName);
											_childen.css('animation-delay', '');
										}, ((_delayIncrease * _childen.length) * 1000) + (_animDelay * 1000) * 3);
									}
								}
							});
						});
					} else {
						// for single container
						_this.css('animation-delay', _delayNum);
						
						// once the target is on display
						$(_this).on('inview', function(event, visible) {
							if(_state) {
								_state = false;

								if(visible) {
									$(_this).addClass(_animName + ' visible');

									// once done, remove everything related to animation
									setTimeout(function() {
										$(_this).removeClass('animate animated anim-content visible');
										$(_this).removeClass(_animName);
										$(_this).css('animation-delay', '');
									}, (_animDelay * 1000) * 4);
								}
							}
						});
					}
				});
			}
		}

		if(state == 'destroy') {
			$('.animate').removeClass('animate');
		} else {
			(function(d){var p={},e,a,h=document,i=window,f=h.documentElement,j=d.expando;d.event.special.inview={add:function(a){p[a.guid+"-"+this[j]]={data:a,$element:d(this)}},remove:function(a){try{delete p[a.guid+"-"+this[j]]}catch(d){}}};d(i).bind("scroll resize",function(){e=a=null});!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){a=null});setInterval(function(){var k=d(),j,n=0;d.each(p,function(a,b){var c=b.data.selector,d=b.$element;k=k.add(c?d.find(c):d)});if(j=k.length){var b;
			if(!(b=e)){var g={height:i.innerHeight,width:i.innerWidth};if(!g.height&&((b=h.compatMode)||!d.support.boxModel))b="CSS1Compat"===b?f:h.body,g={height:b.clientHeight,width:b.clientWidth};b=g}e=b;for(a=a||{top:i.pageYOffset||f.scrollTop||h.body.scrollTop,left:i.pageXOffset||f.scrollLeft||h.body.scrollLeft};n<j;n++)if(d.contains(f,k[n])){b=d(k[n]);var l=b.height(),m=b.width(),c=b.offset(),g=b.data("inview");if(!a||!e)break;c.top+l>a.top&&c.top<a.top+e.height&&c.left+m>a.left&&c.left<a.left+e.width?
			(m=a.left>c.left?"right":a.left+e.width<c.left+m?"left":"both",l=a.top>c.top?"bottom":a.top+e.height<c.top+l?"top":"both",c=m+"-"+l,(!g||g!==c)&&b.data("inview",c).trigger("inview",[!0,m,l])):g&&b.data("inview",!1).trigger("inview",[!1])}}},250)})(jQuery);

			document.addEventListener('DOMContentLoaded', function() {
				_animate.init();
			});
		}
	},
	enquire: function(opt) {
		var _enquire = {
			status: 'once',
			desktop: opt.desktop,
			mobile: opt.mobile,
			counterDesktop: 0,
			counterMobile: 0,
			width: 0,
			between: 810,
			init: function() {
				if(opt.between) {
					_enquire.between = opt.between;
				}

				if(opt.status) {
					_enquire.status = opt.status;
				}

				window.addEventListener('resize', function() {
					_enquire.update();
				});
				_enquire.update();
			},
			update: function() {
				_enquire.width = $(window).outerWidth();

				if(_enquire.status == 'always') {
					// update every resize
					if(_enquire.width > _enquire.between) {
						_enquire.desktop();
					} else {
						_enquire.mobile();
					}
				} else {
					// if detected desktop or mobile then update once
					if(_enquire.width > _enquire.between) {
						_enquire.counterMobile = 0;

						// if desktop
						if(_enquire.counterDesktop == 0) {
							_enquire.counterDesktop = 1;
							_enquire.desktop();
						}
					} else {
						_enquire.counterDesktop = 0;

						// if mobile
						if(_enquire.counterMobile == 0) {
							_enquire.counterMobile = 1;
							_enquire.mobile();
						}
					}
				}
			}
		}
		_enquire.init();
	},
	listHover: function(opt) {
		var _hover = {
			target: '',
			bar: '',
			init: function() {
				_hover.target = opt.target;

				_hover.create();

				var _active = _hover.target.querySelector('.list-hover-set.active');
				if(_active) {
					_hover.update(_active);
					window.addEventListener('resize', function() {
						_hover.update(_active);

						clearTimeout(_hover.updateTimer);
						_hover.resizeTimer = setTimeout(function() {
							_hover.update(_active);
						}, 400);
					});
				}

				var _links = _hover.target.querySelectorAll('.list-hover-set');
				for (var i = 0; i < _links.length; i++) {
					_links[i].addEventListener('mouseenter', function(event) {
						_hover.update(this);
					});
				}

				_hover.target.addEventListener('mouseleave', function(event) {
					_hover.hide();
				});
			},
			create: function() {
				_hover.bar = document.createElement('div');
				_hover.bar.classList.add('list-hover-bar');

				_hover.target.appendChild(_hover.bar);
			},
			updateTimer: 0,
			update: function(target) {
				TweenMax.to(_hover.bar, 0.3, {opacity: 1, y: target.offsetTop, x: target.offsetLeft, width: target.offsetWidth, height: target.offsetHeight});
			},
			hide: function() {
				TweenMax.to(_hover.bar, 0.3, {opacity: 0});
			}
		}
		_hover.init();
	},
	header: function() {
		var _header = {
			target: '',
			menu: '',
			lastScrollTop: 0,
			init: function() {
				_header.target = document.querySelector('.header-content');

				_header.menu = _header.target.querySelector('.menu');
				$('> ul > li', _header.menu).each(function() {
					var _this = this,
					_menuSub = _this.querySelector('.menu-sub');

					if(_menuSub) {
						$(_this).addClass('sub');

						_this.addEventListener('mouseenter', function() {
							if(hfse.width > 810) {
								TweenMax.set(_menuSub, {display: 'block', opacity: 0, x: 40});
								TweenMax.to(_menuSub, 0.3, {opacity: 1, x: 0});
							}
						});
						_this.addEventListener('mouseleave', function() {
							if(hfse.width > 810) {
								TweenMax.to(_menuSub, 0.3, {opacity: 0, x: 40, onComplete: function() {
									TweenMax.set(_menuSub, {clearProps: 'all'});
								}});
							}
						});
					}
				});

				hfse.scrollFn(function() {
					_header.scrolling();
				});

				var _menuParent = _header.menu.parentNode;
				hfse.enquire({
					between: 810,
					desktop: function() {
						_menuParent.appendChild(_header.menu);
					},
					mobile: function() {
						_header.target.querySelector('.he-me-main').appendChild(_header.menu);
					}
				});

				_header.mobile.init();
			},
			scrolling: function() {
				var st = window.pageYOffset || document.documentElement.scrollTop;

				if (st > _header.lastScrollTop) {
					// SCROLL DOWN

					if(st > hfse.getHeight(_header.target)) {
						// add class once the page is scroll down more than the height of the header
						$(_header.target).addClass('header-down');

						// if the scrolling is still on top part of the page
						if($(_header.target).hasClass('header-absolute')) {
							// for desktop, pull up the header once scroll down
							TweenMax.set(_header.target, {y: -(hfse.getHeight(_header.target))});
						} else {
							// for desktop, pull up the header base on header main height
							TweenMax.to(_header.target, 0.3, {y: -(hfse.getHeight(_header.target) + 1)});
						}

						// if the scrolling more than the height of the header, make the header position: fixed
						$(_header.target).removeClass('header-absolute');
					} else {
						$(_header.target).addClass('header-absolute');
					}
				} else {
					// SCROLL UP

					if(st != _header.lastScrollTop) {
						if(st < hfse.getHeight(_header.target)) {
							// if scrolling on the top part of the page
							$(_header.target).removeClass('header-down');
						} else {
							// if scrolling up, show header main
							TweenMax.to(_header.target, 0.3, {y: 0});
						}
					}

					// error catcher once the page accidentally move fast back to top
					if(st == 0) {
						TweenMax.to(_header.target, 0.3, {y: 0});
					}
				}

				_header.lastScrollTop = st <= 0 ? 0 : st;
			},
			mobile: {
				active: false,
				bar: '',
				menu: '',
				menuSub: '',
				btnBack: '',
				init: function() {
					_header.mobile.bar = _header.target.querySelector('.menu-bar');
					_header.mobile.menu = _header.target.querySelector('.header-menu-mobile');
					_header.mobile.menuSub = _header.target.querySelector('.header-menu-sub-mobile');

					_header.mobile.bar.addEventListener('click', function() {
						if(_header.mobile.active) {
							_header.mobile.hide();
						} else {
							_header.mobile.show();
						}
					});

					hfse.resizeFn(function() {
						_header.mobile.resize();
					});

					$('> ul > li', _header.menu).each(function() {
						var _this = this,
						_menuSub = _this.querySelector('.menu-sub');

						if(_menuSub) {
							hfse.enquire({
								between: 810,
								desktop: function() {
									_this.appendChild(_menuSub);
								},
								mobile: function() {
									document.querySelector('.he-me-su-main').appendChild(_menuSub);
								}
							})

							$('> a', _this).click(function(e) {
								if(hfse.width < 810 || hfse.width == 810) {
									e.preventDefault();
									_header.mobile.showSub(_this, _menuSub);
								}
							});
						}
					});

					hfse.enquire({
						between: 810,
						desktop: function() {
							_header.mobile.hideSub();
							_header.mobile.hide();
						},
						mobile: function() {
							_header.mobile.hideSub();
							_header.mobile.hide();
						}
					})
				},
				hide: function() {
					_header.mobile.active = false;
					$(_header.mobile.bar).removeClass('active');

					$('html').removeClass('menu-mobile-active');

					TweenMax.to(_header.mobile.menu, 0.3, {height: 0, onComplete: function() {
						TweenMax.set(_header.mobile.menu, {clearProps: 'all'});
					}});

					TweenMax.to(_header.mobile.menuSub, 0.3, {height: 0, onComplete: function() {
						TweenMax.set(_header.mobile.menuSub, {clearProps: 'all'});
						_header.mobile.hideSub();
					}});
				},
				show: function() {
					_header.mobile.active = true;
					$(_header.mobile.bar).addClass('active');

					$('html').addClass('menu-mobile-active');

					TweenMax.set(_header.mobile.menu, {display: 'block', opacity: 0, height: 0, overflow: 'hidden'});
					TweenMax.to(_header.mobile.menu, 0.3, {opacity: 1, height: window.innerHeight - _header.target.offsetHeight, onComplete: function() {
						$(_header.mobile.menu).css({overflow: ''});
					}});

					TweenMax.set($('.menu > ul > li', _header.mobile.menu), {opacity: 0, y: 30});
					TweenMax.staggerTo($('.menu > ul > li', _header.mobile.menu), 0.3, {opacity: 1, y: 0}, 0.05);
				},
				hideSub: function() {
					if(_header.mobile.btnBack) {
						TweenMax.to(_header.mobile.menuSub, 0.3, {x: window.innerWidth, onComplete: function() {
							_header.mobile.btnBack.remove();
						}});
					}

					TweenMax.to(_header.mobile.menu, 0.3, {x: 0});
				},
				showSub: function(target, sub) {
					_header.mobile.resize();

					var _menuSub = _header.mobile.menuSub.querySelectorAll('.menu-sub');
					for (var i = 0; i < _menuSub.length; i++) {
						if(_menuSub[i] == sub) {
							$(_menuSub[i]).show();
						} else {
							$(_menuSub[i]).hide();
						}
					}

					_header.mobile.btnBack = document.createElement('a');
					$(_header.mobile.btnBack).addClass('link-arrow-left');
					$(_header.mobile.btnBack).html($('a', target).html());
					$('.he-me-su-main', _header.target).prepend(_header.mobile.btnBack);

					_header.mobile.btnBack.addEventListener('click', function(e) {
						e.preventDefault();
						_header.mobile.hideSub();
					});

					TweenMax.to(_header.mobile.menu, 0.3, {x: -40, delay: 0.1});

					TweenMax.set(_header.mobile.menuSub, {display: 'block', x: window.innerWidth});
					TweenMax.to(_header.mobile.menuSub, 0.3, {x: 0});
				},
				resize: function() {
					if(_header.mobile.active) {
						$(_header.mobile.menu).css({height: window.innerHeight - _header.target.offsetHeight})
						$(_header.mobile.menuSub).css({height: window.innerHeight - _header.target.offsetHeight})
					}
				}
			}
		}
		_header.init();
	},
	accordion: function(opt) {
		var _accordion = {
			target: '',
			closeOthers: true,
			openFirst: false,
			list: '',
			init: function() {
				_accordion.target = opt.target;

				if(opt.closeOthers) {
					if(opt.closeOthers == 'true') {
						_accordion.closeOthers = true;
					} else {
						_accordion.closeOthers = false;
					}
				}

				if(opt.openFirst) {
					if(opt.openFirst == 'true') {
						_accordion.openFirst = true;
					} else {
						_accordion.openFirst = false;
					}
				}

				_accordion.list = _accordion.target.querySelectorAll('.com_accordion-set');
				for (var i = 0; i < _accordion.list.length; i++) {
					var _this = _accordion.list[i],
					_title = _this.querySelector('.com_accordion-title');

					_title.addEventListener('click', function(e) {
						e.preventDefault();

						if(this.parentNode.classList.contains('selected')) {
							_accordion.close(this.parentNode);
						} else {
							_accordion.open(this.parentNode);
						}
					});
				}

				// open the default selected sets
				var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');
				if(_activeSet) {
					for (var i = 0; i < _activeSet.length; i++) {
						_accordion.open(_activeSet[i]);
					}
				}

				if(_accordion.openFirst) {
					_accordion.open(_accordion.list[0], 'no-scroll');
				}
			},
			open: function(target, state) {
				if(_accordion.closeOthers) {
					var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');

					if(_activeSet) {
						for (var i = 0; i < _activeSet.length; i++) {
							_accordion.close(_activeSet[i]);
						}
					}
				}

				target.classList.add('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideDown(function() {
					if(state != 'no-scroll') {
						var st = window.pageYOffset || document.documentElement.scrollTop;

						if(_content.offsetTop < st) {
							$('body, html').animate({scrollTop:$(target).offset().top -( hfse.getHeight(document.querySelector('.header-content')) + 15 ) }, 700);
						}
					}
				});
			},
			close: function(target) {
				target.classList.remove('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideUp();
			}
		}
		_accordion.init();
	},
	mobileDropdown: function(opt) {
		var _dd = {
			target: '',
			title: '',
			main: '',
			select: '',
			init: function() {
				_dd.target = opt.target;

				_dd.title = document.createElement('div');
				$(_dd.title).addClass('com_mobile-dropdown-title');
				$(_dd.title).html('Dropdown title goes here');
				$(_dd.target).prepend(_dd.title);

				_dd.main = _dd.target.querySelector('.com_mobile-dropdown-main');
				$(_dd.main).hide();

				_dd.select = document.createElement('select');
				$(_dd.select).addClass('com_mobile-dropdown-select');

				var _links = _dd.target.querySelectorAll('a');
				for (var i = 0; i < _links.length; i++) {
					var _thisLink = _links[i],
					_option = document.createElement('option');

					$(_option).html(_thisLink.innerText);
					_dd.select.appendChild(_option);

					var _active = _thisLink.parentNode.classList.contains('active');
					if(!(_active)) {
						_active = _thisLink.parentNode.classList.contains('ui-state-active');
					}

					if(_active) {
						_dd.update(i, 'set');
					}
				}

				_dd.target.appendChild(_dd.select);

				_dd.select.addEventListener('change', function() {
					if(_dd.title) {
						_dd.update(_dd.select.selectedIndex);
					}
				});

				$(_dd.target).addClass('com_mobile-dropdown-loaded');
			},
			update: function(target, state) {
				var _links = _dd.main.querySelectorAll('a');
				for (var i = 0; i < _links.length; i++) {
					if(i == target) {
						if(state != 'set') {
							_links[i].click();
						}
						$(_dd.title).html(_links[i].innerText);

						_dd.select.selectedIndex = i;
					}
				}
			}
		}

		if(opt.destroy) {
			var _title = opt.target.querySelector('.com_mobile-dropdown-title');

			if(_title) {
				opt.target.removeChild(_title);
				opt.target.classList.remove('com_mobile-dropdown-loaded');

				opt.target.querySelector('.com_mobile-dropdown-main').style.display = '';

				opt.target.removeChild(opt.target.querySelector('.com_mobile-dropdown-select'));
			}
		} else {
			_dd.init();
		}
	},
	mobileScrollable: function(opt) {
		var _scroll = {
			target: '',
			columns: 2,
			init: function() {
				_scroll.target = opt.target;

				if(opt.columns) {
					_scroll.columns = opt.columns;
				}

				$(_scroll.target).addClass('com_mobile-scollable-loaded');

				hfse.resizeFn(function() {
					_scroll.resize();
				});
			},
			resize: function() {
				$(_scroll.target).css({minWidth: ''});

				if(_scroll.target.classList.contains('com_mobile-scollable-loaded')) {
					var _width = ( (window.innerWidth * 0.33) - 20 ) * _scroll.columns;

					if(hfse.width < 480 || hfse.width == 480) {
						_width = ( (window.innerWidth * 0.5) - 30 ) * _scroll.columns;						
					}

					$('> div', _scroll.target).css({minWidth: _width});
				}
			}
		}

		if(opt.destroy) {
			opt.target.classList.remove('com_mobile-scollable-loaded');
			$('> div', opt.target).css({minWidth: ''});
		} else {
			_scroll.init();
		}
	}
}
hfse.init();